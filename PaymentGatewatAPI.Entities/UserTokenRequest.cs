﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PaymentGatewatAPI.Entities
{
    public class UserTokenRequest
    {
        public UserTokenRequest()
        {
            this.BillingAddressModel = new BillingAddressModel();
        }

        [Required]
        public string MerchantUserName { get; set; }

        [Required]
        public string MerchantPassword { get; set; }

        [Required]
        public string InvoiceNumber { get; set; }

        [Required]
        public double Amount { get; set; }

        [Required]
        public string Currency { get; set; }

        [Required]
        public BillingAddressModel BillingAddressModel { get; set; }
    }

    public class BillingAddressModel
    {
        [Required]
        public string Line1 { get; set; }

        [Required]
        public string Line2 { get; set; }

        [Required]
        public string City { get; set; }

        [Required]
        public string PostalCode { get; set; }

        [Required]
        public string State { get; set; }

        [Required]
        public string Country { get; set; }
    }
}
