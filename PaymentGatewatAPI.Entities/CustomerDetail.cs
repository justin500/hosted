﻿using PaymentGatewayAPI.FatZebra.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PaymentGatewatAPI.Entities
{
    public class CustomerDetail
    {
        public CustomerDetail()
        {
            this.CardDetail = new CardDetail();
            this.PaymentDetails = new PaymentDetails();
        }

        public CardDetail CardDetail { get; set; }

        public PaymentDetails PaymentDetails { get; set; }

        public BillingAddressModel BillingAddress { get; set; }

        public bool IsPaymentDone { get; set; }

        public string UniqueId { get; set; }

        public Purchase PaymentResponse { get; set; }

        public string ReturnUrl { get; set; }
    }

    public class PaymentDetails
    {
        [Display(Name = "merchantReferenceNumber")]
        public string MerchantReference { get; set; }

        public double Amount { get; set; }

        public string Currency { get; set; }
    }

    public class CardDetail
    {
        [Required(ErrorMessage = "Card is required.")]
        [Range(100000000000, 9999999999999999999, ErrorMessage = "Card number must be between 12 and 19 digits.")]
        public string CardNumber { get; set; }

        [Required(ErrorMessage = "Card Holder Name is required.")]
        public string CardHolderName { get; set; }

        [Required(ErrorMessage = "Expiry month is required.")]
        [RegularExpression("^[0-9]{2}$", ErrorMessage = "Expiry month is invalid.")]
        public int ExpiryDateMM { get; set; }

        [Required(ErrorMessage = "Expiry year is required.")]
        [RegularExpression("^[0-9]{2}$", ErrorMessage = "Expiry year is invalid.")]
        public int ExpiryDateYY { get; set; }
            
        public int IssueNumber { get; set; }

        [Required(ErrorMessage = "CVV is required.")]
        [RegularExpression("^[0-9]{3,4}$", ErrorMessage = "Invalid CVV number.")]
        public int CVV { get; set; }


        [IsTrueAttribute(ErrorMessage = "Please select.")] 
        public bool IsChecked { get; set; }
    }
}
