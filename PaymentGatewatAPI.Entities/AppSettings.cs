﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGatewatAPI.Entities
{
    public class AppSettings
    {
        public string Secret { get; set; }

        /// <summary>
        /// The API Username
        /// </summary>
        public  string Gateway_Username { get; set; }

        /// <summary>
        /// The API Token
        /// </summary>
        public string Gateway_Token { get; set; }

        /// <summary>
        /// The API Token
        /// </summary>
        public bool Gateway_SandboxMode { get; set; }

        /// <summary>
        /// The API Token
        /// </summary>
        public bool Gateway_TestMode { get; set; }
    }
}
