﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGatewatAPI.Entities
{
    public class ResponseObject<T> where T : class
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="JSONClassResult{T}" /> class.
        /// </summary>
        /// <param name="classObject">class Object</param>
        public ResponseObject(T classObject)
        {
            this.Data = classObject;
        }

        public bool Status { get; set; }

        public string Message { get; set; }

        public T Data { get; set; }
    }
}
