﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGatewatAPI.Entities
{
    public class UserTokenResponse
    {
        public string Token { get; set; }
    }
}
