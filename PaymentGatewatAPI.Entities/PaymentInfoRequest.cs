﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace PaymentGatewatAPI.Entities
{
    public class PaymentInfoRequest
    {
        [Required]
        public string MerchantUserName { get; set; }

        [Required]
        public string MerchantPassword { get; set; }

        [Required]
        public string ReferenceNumber { get; set; }
    }
}
