﻿using PaymentGatewatAPI.BLL.Interface;
using PaymentGatewatAPI.DLL.Interface;
using PaymentGatewatAPI.Entities;
using PaymentGatewayAPI.FatZebra.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGatewatAPI.BLL.Implementation
{
    public class UserComponent : IUserComponent
    {
        private IUserDataAccess _userObj;
        public UserComponent(IUserDataAccess user)
        {
            _userObj = user;
        }

        public ResponseObject<UserTokenResponse> SaveUserTokenRequest(UserTokenRequest request)
        {
            ResponseObject<UserTokenResponse> res = new ResponseObject<UserTokenResponse>(new UserTokenResponse());
            try
            {
                // Login related to business here

                // After this send request DL
                res = _userObj.SaveUserTokenRequest(request);
            }
            catch (Exception)
            {
                throw;
            }
            return res;
        }


        public ResponseObject<UserTokenRequest> GetRequestObject(string id, string merchantReferenceNumber)
        {            
            try
            {
                return _userObj.GetRequestObject(id, merchantReferenceNumber);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdatePaymentStatus(string id, string merchantReferenceNumber, Response<Purchase> purchaseResponse)
        {
            try
            {
                return _userObj.UpdatePaymentStatus(id, merchantReferenceNumber, purchaseResponse);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResponseObject<Response<Purchase>> GetResponseObject(string merchantReferenceNumber)
        {
            try
            {
                return _userObj.GetResponseObject(merchantReferenceNumber);
            }
            catch (Exception)
            {
                throw;
            }
        }

        
    }
}
