﻿using PaymentGatewatAPI.Entities;
using PaymentGatewayAPI.FatZebra.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace PaymentGatewatAPI.BLL.Interface
{
    public interface IUserComponent
    {
        ResponseObject<UserTokenResponse> SaveUserTokenRequest(UserTokenRequest request);

        ResponseObject<UserTokenRequest> GetRequestObject(string id, string merchantReferenceNumber);

        bool UpdatePaymentStatus(string id, string merchantReferenceNumber, Response<Purchase> purchaseResponse);

        ResponseObject<Response<Purchase>> GetResponseObject(string merchantReferenceNumber);
    }
}
