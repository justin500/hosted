﻿using System;

namespace PaymentGatewayAPI.FatZebra.Models
{
	public enum FraudResult
	{
		Unknown,
		Accept,
		Challenge,
		Deny,
		Error
	}
}

