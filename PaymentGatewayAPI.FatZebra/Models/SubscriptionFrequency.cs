﻿using System;

namespace PaymentGatewayAPI.FatZebra.Models
{
	public enum SubscriptionFrequency
	{
		Daily,
		Weekly,
		Fortnightly,
		Monthly,
		Quarterly,
		BiAnnually,
		Annually
	}
}

