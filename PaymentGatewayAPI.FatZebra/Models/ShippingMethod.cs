﻿using System;
using Newtonsoft.Json;

namespace PaymentGatewayAPI.FatZebra.Models
{
	public enum ShippingMethod
	{
		LowCost,
		SameDay,
		Overnight,
		Express,
		International,
		Pickup,
		Other
	}
}

