﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace PaymentGatewayAPI.FatZebra.Models
{
    public interface IRecord
    {
		[JsonProperty("id")] 
		string ID { get; }
		[JsonProperty("successful")]
		bool Successful { get; }
    }
}
