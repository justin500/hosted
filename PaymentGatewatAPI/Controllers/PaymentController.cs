﻿using Microsoft.AspNetCore.Mvc;
using PaymentGatewatAPI.BLL.Interface;
using PaymentGatewatAPI.Entities;
using PaymentGatewatAPI.Services;
using PaymentGatewayAPI.FatZebra.Models;
using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;

namespace PaymentGatewatAPI.Controllers
{
    public class PaymentController : Controller
    {
        private IUserComponent _userComponent;
        private ITokenService _tokenService;

        public PaymentController(IUserComponent userComponent, ITokenService tokenService)
        {
            _userComponent = userComponent;
            _tokenService = tokenService;
        }

        [HttpPost]
        public IActionResult Transaction(string tokenKey, string merchantReferenceNumber, string returnUrl)
        {
            try
            {
                //  merchantReferenceNumber = Guid.NewGuid().ToString();
                var uiqueId = _tokenService.ValidateToken(tokenKey)?.FindFirst(ClaimTypes.SerialNumber)?.Value;

                if (uiqueId != null)
                {
                    var response = _userComponent.GetRequestObject(uiqueId, merchantReferenceNumber);

                    if (response.Status)
                    {
                        CustomerDetail customerDetail = new CustomerDetail()
                        {
                            CardDetail = new CardDetail()
                            {
                                CardNumber = "5123456789012346",
                                CardHolderName = "M Smith",
                                CVV = 123,
                                ExpiryDateMM = 11,
                                ExpiryDateYY = 22,
                                IsChecked = false
                            },
                            PaymentDetails = new PaymentDetails()
                            {
                                MerchantReference = merchantReferenceNumber,
                                Amount = response.Data.Amount,
                                Currency = response.Data.Currency,
                            },
                            BillingAddress = response.Data.BillingAddressModel,
                            IsPaymentDone = false,
                            UniqueId = uiqueId,
                            ReturnUrl= returnUrl
                        };

                        return View(customerDetail);
                    }
                    else // payment processed or any other error
                    {
                        return View("ErrorPage", new ErrorPageModel { ErrorMessage = response.Message });
                    }
                } // token invalid
                else
                {
                    return View("ErrorPage", new ErrorPageModel { ErrorMessage = "Invalid Token." });
                }
            }
            catch
            {
                return View("ErrorPage", new ErrorPageModel { ErrorMessage = "An error occurred while processing your request." });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessTransaction(CustomerDetail customerDetail)
        {
            try
            {
                // 1. Validate the mdoel
                if (ModelState.IsValid)
                {
                    // 2. Do the payment and send the required parameter
                    var response = Purchase.Create(Convert.ToInt32(customerDetail.PaymentDetails.Amount), customerDetail.CardDetail.CardHolderName, customerDetail.CardDetail.CardNumber, new DateTime(CultureInfo.CurrentCulture.Calendar.ToFourDigitYear(customerDetail.CardDetail.ExpiryDateYY), customerDetail.CardDetail.ExpiryDateMM, 1), customerDetail.CardDetail.CVV.ToString(), customerDetail.PaymentDetails.MerchantReference, "0.0.0.1", customerDetail.PaymentDetails.Currency);
                    if (response.Successful)
                    {
                        // 3. Update payment status as IsPaymentDone:True
                        _userComponent.UpdatePaymentStatus(customerDetail.UniqueId, customerDetail.PaymentDetails.MerchantReference, response);
                        customerDetail.IsPaymentDone = true;

                        // 4. Set payment response
                        customerDetail.PaymentResponse = response.Result;

                        return View("Transaction", customerDetail);
                    }
                    else // Error if payment failed
                    {
                        _userComponent.UpdatePaymentStatus(customerDetail.UniqueId, customerDetail.PaymentDetails.MerchantReference, response);
                        TempData["CustomMessage"] = string.Join("." + Environment.NewLine, response.Errors);
                        return View("Transaction", customerDetail);
                    }
                }
                else
                {
                    return View("Transaction", customerDetail);
                }
            }
            catch (Exception)
            {
                return View("ErrorPage", new ErrorPageModel { ErrorMessage = "An error occurred while processing your request." });
            }
        }

        public IActionResult PaymentSuccessful(string referenceNumber)
        {
            try
            {
                var response = Purchase.Get(referenceNumber);
                return View(null);
            }
            catch
            {
                return View("ErrorPage", new ErrorPageModel { ErrorMessage = "An error occurred while processing your request." });
            }
        }
    }
}
