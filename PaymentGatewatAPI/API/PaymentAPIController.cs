﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PaymentGatewatAPI.BLL.Interface;
using PaymentGatewatAPI.Entities;
using PaymentGatewatAPI.Services;
using System;
using PaymentGatewayAPI.FatZebra.Models;
using System.Linq;

namespace PaymentGatewatAPI.API
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class PaymentAPIController : ControllerBase
    {
        private readonly IUserComponent _userComponent;
        private readonly ITokenService _tokenService;

        public PaymentAPIController(IUserComponent userComponent, ITokenService tokenService)
        {
            _userComponent = userComponent;
            _tokenService = tokenService;
        }

        [AllowAnonymous]
        [HttpPost("GenerateToken")]
        public ResponseObject<UserTokenResponse> GenerateToken(UserTokenRequest userTokenRequest)
        {
            ResponseObject<UserTokenResponse> res = new ResponseObject<UserTokenResponse>(new UserTokenResponse());

            try
            {
                // 1. Check the merchat user name password with DB ::TODO

                // 2. Store basic request info into database
                var userObj = _userComponent.SaveUserTokenRequest(userTokenRequest);

                // 3. If request saved successfully, generate unique id and add it to claims inside token 
                // Which will be use in hosted page for getting the info from db
                if (userObj.Status)
                {
                    userObj.Data.Token = _tokenService.GenereateToken(userObj.Message);
                    res.Data = userObj.Data;
                    res.Status = true;
                }
                else
                {
                    res.Message = userObj.Message;
                    res.Status = false;
                }
            }
            catch (Exception ex)
            {
                res.Status = false;
                res.Message = ex.Message;
            }
            return res;
        }

        [AllowAnonymous]
        [HttpPost("GetPaymentInfoFromGateway")]
        public ResponseObject<Response<Purchase>> GetPaymentInfoFromGateway(PaymentInfoRequest paymentInfoRequest)
        {
            ResponseObject<Response<Purchase>> res = new ResponseObject<Response<Purchase>>(new Response<Purchase>());

            try
            {
                // 1. ::TODO Check the merchat user name password with DB 

                // 2. Get payment response
                var purchaseResponse = Purchase.Get(paymentInfoRequest.ReferenceNumber);
                if (purchaseResponse.Successful)
                {
                    res.Status = true;

                    res.Data.Successful = purchaseResponse.Successful;
                    res.Data.Errors = purchaseResponse.Errors;
                    res.Data.IsTest = purchaseResponse.IsTest;
                    res.Data.Result = purchaseResponse.Result.Where(s => s.Reference == paymentInfoRequest.ReferenceNumber).FirstOrDefault();
                }
                else
                {
                    res.Status = false;
                    res.Message = string.Join(",", purchaseResponse.Errors);
                }
            }
            catch (Exception ex)
            {
                res.Status = false;
                res.Message = ex.Message;
            }

            return res;
        }

        [AllowAnonymous]
        [HttpPost("GetPaymentInfo")]
        public ResponseObject<Response<Purchase>> GetPaymentInfo(PaymentInfoRequest paymentInfoRequest)
        {
            ResponseObject<Response<Purchase>> res = new ResponseObject<Response<Purchase>>(new Response<Purchase>());

            try
            {
                // 1. ::TODO Check the merchat user name password with DB 

                // 2. Get payment response
                var purchaseResponse = _userComponent.GetResponseObject(paymentInfoRequest.ReferenceNumber);
                if (purchaseResponse.Status)
                {
                    res.Data = purchaseResponse.Data;
                    res.Status = true;
                }
                else
                {
                    res.Status = false;
                    res.Message = purchaseResponse.Message;
                }
            }
            catch (Exception ex)
            {
                res.Status = false;
                res.Message = ex.Message;
            }

            return res;
        }
    }
}
