﻿using Newtonsoft.Json;
using PaymentGatewatAPI.DataModel.Models;
using PaymentGatewatAPI.DLL.Interface;
using PaymentGatewatAPI.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using PaymentGatewayAPI.FatZebra.Models;

namespace PaymentGatewatAPI.DLL.Implementation
{
    public class UserDataAccess : IUserDataAccess
    {
        public ResponseObject<UserTokenResponse> SaveUserTokenRequest(UserTokenRequest request)
        {
            ResponseObject<UserTokenResponse> res = new ResponseObject<UserTokenResponse>(new UserTokenResponse());
            try
            {
                // Check the User credentials with DB
                using (var context = new PaymentGatewatDBContext())
                {
                    var uniqueId = Guid.NewGuid().ToString();
                    context.Add(new TokenRequest() { Id = uniqueId, IsPaymentDone = false, CreatedDate = DateTime.UtcNow, RequestObject = JsonConvert.SerializeObject(request) });
                    context.SaveChanges();
                    res.Status = true;
                    res.Message = uniqueId;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return res;
        }

        public ResponseObject<UserTokenRequest> GetRequestObject(string id, string merchantReferenceNumber)
        {
            ResponseObject<UserTokenRequest> res = new ResponseObject<UserTokenRequest>(new UserTokenRequest());
            try
            {
                using (var context = new PaymentGatewatDBContext())
                {
                    TokenRequest tokenRequest = context.TokenRequests.Where(s => s.Id == id).FirstOrDefault();
                    if (tokenRequest != null)
                    {
                        if (Convert.ToBoolean(tokenRequest.IsPaymentDone) && tokenRequest.MerchantRererence == merchantReferenceNumber)
                        {
                            res.Status = false;
                            res.Message = "Payment already done.";
                        }
                        else
                        {
                            res.Data = JsonConvert.DeserializeObject<UserTokenRequest>(tokenRequest.RequestObject);
                            res.Status = true;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return res;
        }

        public bool UpdatePaymentStatus(string id, string merchantReferenceNumber, Response<Purchase> purchaseResponse)
        {
            ResponseObject<UserTokenRequest> res = new ResponseObject<UserTokenRequest>(new UserTokenRequest());
            try
            {
                using (var context = new PaymentGatewatDBContext())
                {
                    TokenRequest tokenRequest = context.TokenRequests.Where(s => s.Id == id).FirstOrDefault();
                    if (tokenRequest != null)
                    {
                        tokenRequest.IsPaymentDone = purchaseResponse.Successful;
                        tokenRequest.MerchantRererence = merchantReferenceNumber;
                        tokenRequest.PaymentGatewayResponse = JsonConvert.SerializeObject(purchaseResponse);
                        tokenRequest.ModifiedDate = DateTime.UtcNow;
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ResponseObject<Response<Purchase>> GetResponseObject(string merchantReferenceNumber)
        {
            ResponseObject<Response<Purchase>> res = new ResponseObject<Response<Purchase>>(new Response<Purchase>());
            try
            {
                using (var context = new PaymentGatewatDBContext())
                {
                    TokenRequest tokenRequest = context.TokenRequests.Where(s => s.MerchantRererence == merchantReferenceNumber).FirstOrDefault();
                    if (tokenRequest != null)
                    {

                        res.Data = JsonConvert.DeserializeObject<Response<Purchase>>(tokenRequest.PaymentGatewayResponse);
                        res.Status = true;
                    }
                    else
                    {
                        res.Status = false;
                        res.Message = "Payment status is not updated.";
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return res;
        }

    }
}
