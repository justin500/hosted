﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace PaymentGatewatAPI.DataModel.Models
{
    public partial class PaymentGatewatDBContext : DbContext
    {
        public PaymentGatewatDBContext()
        {
        }

        public PaymentGatewatDBContext(DbContextOptions<PaymentGatewatDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TokenRequest> TokenRequests { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                //optionsBuilder.UseSqlServer("Data Source=.;initial catalog=PaymentTransactions;Integrated Security=True;ConnectRetryCount=0");
                optionsBuilder.UseSqlServer("Data Source=GATEWAY\\VISIONPAY;initial catalog=PaymentTransactions;User ID=sa;Password=M,ZFaVK2V6&TF+*!;ConnectRetryCount=0");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<TokenRequest>(entity =>
            {
                entity.Property(e => e.Id).IsUnicode(false);

                entity.Property(e => e.MerchantRererence).IsUnicode(false);

                entity.Property(e => e.PaymentGatewayResponse).IsUnicode(false);

                entity.Property(e => e.RequestObject).IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
