﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

#nullable disable

namespace PaymentGatewatAPI.DataModel.Models
{
    [Table("TokenRequest")]
    public partial class TokenRequest
    {
        [Key]
        [StringLength(50)]
        public string Id { get; set; }
        public string RequestObject { get; set; }
        public bool? IsPaymentDone { get; set; }
        [StringLength(50)]
        public string MerchantRererence { get; set; }
        public string PaymentGatewayResponse { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? CreatedDate { get; set; }
        [Column(TypeName = "datetime")]
        public DateTime? ModifiedDate { get; set; }
    }
}
